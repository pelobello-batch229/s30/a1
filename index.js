
// 1. Create an index.js file in a new a1 folder on where to write and save
// the solution for the activity.
// 2. Aggregate to count the total number of items supplied by Yellow Farms
// and has a price less than 50. ($count stage)
db.fruits.aggregate([

    {$match:{$and:[
        
            {supplier:"Yellow Farms"},
            {price:{$lt:50}}
            
     ]}},

    {$count:"TotalSuppliedItemsOfYellowFarm>50"}

])


// 3. Aggregate to count the total number of items with price lesser than 30.
// ($count stage)
db.fruits.aggregate([
    
     
    {$match:{price:{$lt:30}}},
    {$count: "TotalItemswithPrice>30"}

])


// 4. Aggregate to get the average price of fruits supplied by Yellow Farm.
// ($group)
db.fruits.aggregate([
    
      
    {$match:{supplier:"Yellow Farms"}},
    {$group:{_id:"Yellow Farm", avegPrice: {$avg: "$price"}}}

])

// 5. Aggregate to get the highest price of fruits supplied by Red Farm Inc. ($group)
db.fruits.aggregate([
    
      
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.", HighestPrice: {$max: "$price"}}}

])

// 6. Aggregate to get the lowest price of fruits supplied by Red Farm Inc. ($group)
db.fruits.aggregate([
    
      
    {$match:{supplier:"Red Farms Inc."}},
    {$group:{_id:"Red Farms Inc.", LowestPrice: {$min: "$price"}}}

])